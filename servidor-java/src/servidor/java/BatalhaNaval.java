/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class BatalhaNaval {

    ArrayList<Jogador> jogadores = new ArrayList<>();
    HashMap<String, ArrayList<String>> jogos = new HashMap<>();
    HashMap<String, String> notificacoesTemporarias = new HashMap<>();

    /**
     * Registra o jogador na lista de jogadores.
     *
     * @param nome  Nome do jogador a ser registrado
     * @param mapa  Mapa criado pelo jogador
     */
    public void registrar(@WebParam(name = "nome") String nome, @WebParam(name = "mapa") String mapa) {
        Jogador jogador = new Jogador();
        jogador.nome = nome;
        jogador.mapa = mapa;

        jogadores.add(jogador);
    }

    /**
     * Vincula dois jogadores cadastrados, para iniciar uma nova partida.
     *
     * @param nome  Nome do jogador
     * @param enemy Nome do oponente
     */
    public void vincular(@WebParam(name = "nome") String nome, @WebParam(name = "enemy") String enemy) {
        //Vincula jogador1
        Jogador jogador1 = jogadores.get(searchJogador(nome));
        jogador1.enemy = enemy;
        jogador1.turn = 1;
        jogadores.set(searchJogador(nome), jogador1);

        //Vincula jogador2
        Jogador jogador2 = jogadores.get(searchJogador(enemy));
        jogador2.enemy = nome;
        jogadores.set(searchJogador(enemy), jogador2);

        //Registra o mapa de cada jogador para condição de vitória
        jogos.put(jogador1.nome, new ArrayList<>(Arrays.asList(jogador1.mapa.split("-"))));
        jogos.put(jogador2.nome, new ArrayList<>(Arrays.asList(jogador2.mapa.split("-"))));

        //Registra notificações temporárias
        notificacoesTemporarias.put(jogador1.nome, "");
        notificacoesTemporarias.put(jogador2.nome, "");
    }

    /**
     * Obtém a lista completa com todos os jogadores (clientes) conectados ao servidor, excluindo o cliente passado
     * como parâmetro.
     *
     * @param nome  Nome do cliente a ser desconsiderado a lista de jogadores
     * @return  Retorna a lista com todos os jogadores, menos o jogador passado como parâmetro
     */
    public String getClientes(@WebParam(name = "nome") String nome) {
        String resposta = "";
        ArrayList<String> nomeJogadores = getNomeJogadores();

        System.out.println(nome);

        nomeJogadores.remove(nome);

        for (String nomeJogador : nomeJogadores) {
            resposta += "-" + nomeJogador;
        }

        return resposta;
    }

    /**
     * Verifica se o jogador foi desafiado por algum adversário.
     *
     * @param nome  Nome do jogador, para verificar se ele foi desafiado
     * @return  Retorna texto "true" se ele foi desafiado ou retorna texto "false" caso o jogador ainda não tenha nehum
     *          oponente
     */
    public String isDesafiado(@WebParam(name = "nome") String nome) {
        Jogador jogador = jogadores.get(searchJogador(nome));
        if (!jogador.enemy.equals("")) {
            return "true";
        }

        return "false";
    }

    /**
     * Verifica se é o turno do jogador no momento.
     *
     * @param nome  Nome do jogador a ser verificado o turno
     * @return  Retorna texto "true" se é o turno do jogador no momento ou retorna texto "false" caso não seja o turno do
     *          jogador no momento
     */
    public String verificaTurno(@WebParam(name = "nome") String nome) {
        Jogador jogador = jogadores.get(searchJogador(nome));
        if (jogador.turn == 1) {
            return "true";
        }

        return "false";
    }

    /**
     * Obtém o mapa criado pelo jogador.
     *
     * @param nome  Nome do jogador que é para buscar o mapa
     * @return  Retorna o mapa criado pelo jogador passado como parâmetro
     */
    public String getMapa(@WebParam(name = "nome") String nome) {
        Jogador jogador = jogadores.get(searchJogador(nome));

        return jogador.mapa;
    }

    /**
     * Ataca adversário em uma dada posição do mapa.
     *
     * @param nome  Nome do jogador que está realizando o ataque
     * @param value Posição do mapa a ser atacada.
     * @return Returna o texto "true" se algum alvo é atingido ou o texto "false" se nada foi acertado
     */
    public String atacar(@WebParam(name = "nome") String nome, @WebParam(name = "value") String value) {
        //Obtém jogadores
        Jogador jogador = jogadores.get(searchJogador(nome));
        Jogador enemy = jogadores.get(searchJogador(jogador.enemy));

        //Passa o turno do atacante
        jogador.turn = 0;
        enemy.turn = 1;

        //Obtém o mapa do oponente
        ArrayList<String> mapa = jogos.get(enemy.nome);

        System.out.println("valor:" + value);

        //Verifica se o mapa algo no local atacado
        if (mapa.contains(value)) {
            System.out.println(value);

            //Remove ponto do mapa.
            mapa.remove(value);

            //Adiciona notificação para o outro jogador
            notificacoesTemporarias.put(enemy.nome, "acertado");

            //Retorna texto de sucesso
            return "true";
        }

        //Retorna texto de que nada foi acertado
        return "false";
    }

    /**
     * Obtém notificações destinada a um jogador. Sendo possível a partir disso transmitir mensagens especiais entre
     * jogadores.
     *
     * @param nome  Nome do jogador que se pretenden obter as mensagens
     * @return  Texto da notificação
     */
    public String notificaoesDeJogo(@WebParam(name = "nome") String nome) {
        //Obtém jogador
        Jogador jogador = jogadores.get(searchJogador(nome));

        //Obtém notificação registrada
        String notificao = notificacoesTemporarias.get(jogador.nome);

        //Apaga notificação para evitar duplicação de mensagens transmitidas
        notificacoesTemporarias.put(jogador.nome, "");

        return notificao;
    }

    /**
     * Verifica se o jogo termino. Obtendo o nome do jogador que venceu a partida.
     *
     * @param nome  Nome do jogador que está jogando.
     * @return  Retorna o nome do jogador que venceu a partida em caso de vitória ou o texto "false" caso a partida ainda
     *          não tenha nenhum jogador.
     */
    public String fimDeJogo(@WebParam(name = "nome") String nome) {
        Jogador jogador = jogadores.get(searchJogador(nome));
        Jogador enemy = jogadores.get(searchJogador(jogador.enemy));

        ArrayList<String> mapaJogador = jogos.get(jogador.nome);
        ArrayList<String> mapaEnemy = jogos.get(enemy.nome);

        if (mapaJogador.isEmpty() || mapaEnemy.isEmpty()) {
            //Obtém nome do ganhador
            return mapaJogador.isEmpty() ? enemy.nome : jogador.nome;
        }

        //Ninguém ganho ainda
        return "false";
    }

    //Obtém uma lista com o nome de todos os jogadores cadastrados no servidor nesse momento.
    private ArrayList<String> getNomeJogadores() {
        ArrayList<String> nomeJogadores = new ArrayList<>();

        //Cria uma lista com o nome de todos os jogadores cadastrados
        for (Jogador jogador: jogadores){
            nomeJogadores.add(jogador.getNome());
        }

        return nomeJogadores;
    }

    //Busca index do jogador na lista de jogadores pelo nome
    private int searchJogador(String name) {
        int i = 0;
        for (Jogador jogador : jogadores) {
            if (jogador.nome.equals(name)) {
                return i;
            }
            i++;
        }

        return -1;
    }
}
