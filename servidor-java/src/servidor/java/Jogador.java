/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor.java;

/**
 *
 * @author Douglas
 */
public class Jogador {

    String nome;
    String mapa;
    String enemy;
    int turn;

    public Jogador() {
        this.nome = "";
        this.mapa = "";
        this.enemy = "";
        this.turn = 0;
    }

    public String getNome() {
        return nome;
    }

    public String getMapa() {
        return mapa;
    }

    public String getEnemy() {
        return enemy;
    }

}
