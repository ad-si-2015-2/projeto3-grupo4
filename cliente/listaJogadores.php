<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include 'import/head.html'; ?>
        <?php
        //Sem parametros de jogos redireciona para a página index.php
        $nome = isset($_GET['name']) ? $_GET['name'] : NULL;

        if (empty($nome)) {
            header("Location: index.php");
            exit();
        }
        ?>
    </head>
    <body>
        <div id="outer">  
            <div id="inner">
                <div class="box">
                    <form id="adversarioForm">
                        <input type="hidden" id="name" name="name" value="<?php echo $_GET["name"]; ?>"/>
                        <label>Escolha um adversário para duelar ou aguarde nesta tela até alguem te desafiar:</label>
                        <select id="selectJogador" name="enemy" class="form-control">
                            <option>Carregando...</option>
                        </select>
                        <button class="btn btn-default pull-right" 
                                style="margin: 10px 0px 10px 0px;" id="desafiar" 
                                type="button">Desafiar</button>
                    </form>
                </div>
            </div>
        </div>

    <?php include 'import/scripts.html'; ?>
    <script type="text/javascript">
        $('#desafiar').on('click', function () {
            $.post("requests/vinculaJogadores.php", $("#adversarioForm").serialize()).done(function (data) {
                if (data === "true") {
                    $(location).attr('href', 'game.php?name=' + $('#name').val());
                }
            });
        });

        function atualizarSelectJogadores() {
            $.get("requests/getClientes.php?name=" + $('#name').val(), function (data) {
                var res = data.split("-");
                var combo = $("#selectJogador");
                var options = "";

                //Remove espaço em branco da lista de jogadores
                res = res.filter(function(texto) {
                    return texto !== "";
                });

                //Caso tenha outros jogadores conectados
                if (res.length > 0) {
                    $.each(res, function () {
                        options += '<option value="' + this + '">' + this + '</option>';
                    });
                } else {
                    //Mensagem para jogador aguardar outros jogadores conectarem
                    options += '<option>Sem jogadores, aguarde...</option>';
                }

                //Adiciona mensagem na página
                combo.html(options);
            });
        }

        function isDesafiado() {
            $.get("requests/isDesafiado.php?name=" + $('#name').val(), function (data) {
                //Verifica se o jogador foi desafiado, iniciando o jogo
                if (data === "true") {
                    alert("Você foi desafiado! O jogo irá começar!");

                    //Redirecionando para o jogo
                    $(location).attr('href', 'game.php?name=' + $('#name').val());
                }
            });
        }

        // Definindo intervalo que a função será chamada
        setInterval("isDesafiado()", 7000);
        setInterval("atualizarSelectJogadores()", 7000);

        // Quando carregar a página
        $(function () {
            // Faz a primeira atualização
            atualizarSelectJogadores();
        });
    </script>
</body>
</html>