<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include 'import/head.html'; ?>
        <?php
        //Sem parametros de jogos redireciona para a página index.php
        $nome = isset($_GET['name']) ? $_GET['name'] : NULL;

        if (empty($nome)) {
            header("Location: index.php");
            exit();
        }
        ?>
    </head>
    <body>
        <div id="outer">  
            <div id="inner" style="width:800px !important;">
                <div class="box" id="yourMap">
                    <label>Seu mapa</label>
                    <br>
                    <input type="checkbox" value="1" name="yourMap[1]">
                    <input type="checkbox" value="2" name="yourMap[2]">
                    <input type="checkbox" value="3" name="yourMap[3]">
                    <input type="checkbox" value="4" name="yourMap[4]">
                    <input type="checkbox" value="5" name="yourMap[5]">
                    <input type="checkbox" value="6" name="yourMap[6]">
                    </br>                       
                    <input type="checkbox" value="7" name="yourMap[7]">
                    <input type="checkbox" value="8" name="yourMap[8]">
                    <input type="checkbox" value="9" name="yourMap[9]">
                    <input type="checkbox" value="10" name="yourMap[10]">
                    <input type="checkbox" value="11" name="yourMap[11]">
                    <input type="checkbox" value="12" name="yourMap[12]">
                    </br>                        
                    <input type="checkbox" value="13" name="yourMap[13]">
                    <input type="checkbox" value="14" name="yourMap[14]">
                    <input type="checkbox" value="15" name="yourMap[15]">
                    <input type="checkbox" value="16" name="yourMap[16]">
                    <input type="checkbox" value="17" name="yourMap[17]">
                    <input type="checkbox" value="18" name="yourMap[18]">
                    </br>                        
                    <input type="checkbox" value="19" name="yourMap[19]">
                    <input type="checkbox" value="20" name="yourMap[20]">
                    <input type="checkbox" value="21" name="yourMap[21]">
                    <input type="checkbox" value="22" name="yourMap[22]">
                    <input type="checkbox" value="23" name="yourMap[23]">
                    <input type="checkbox" value="24" name="yourMap[24]">
                    </br>                        
                    <input type="checkbox" value="25" name="yourMap[25]">
                    <input type="checkbox" value="26" name="yourMap[26]">
                    <input type="checkbox" value="27" name="yourMap[27]">
                    <input type="checkbox" value="28" name="yourMap[28]">
                    <input type="checkbox" value="29" name="yourMap[29]">
                    <input type="checkbox" value="30" name="yourMap[30]">
                    </br>                        
                    <input type="checkbox" value="31" name="yourMap[31]">
                    <input type="checkbox" value="32" name="yourMap[32]">
                    <input type="checkbox" value="33" name="yourMap[33]">
                    <input type="checkbox" value="34" name="yourMap[34]">
                    <input type="checkbox" value="35" name="yourMap[35]">
                    <input type="checkbox" value="36" name="yourMap[36]">
                </div>
                <div class="box" id="enemyMap">
                    <label>Mapa inimigo</label>
                    <br>
                    <input type="hidden" id="name" name="name" value="<?php echo $nome; ?>"/>
                    <input type="checkbox" value="1" name="enemyMap[1]" onclick="atacar(this);">
                    <input type="checkbox" value="2" name="enemyMap[2]" onclick="atacar(this);">
                    <input type="checkbox" value="3" name="enemyMap[3]" onclick="atacar(this);">
                    <input type="checkbox" value="4" name="enemyMap[4]" onclick="atacar(this);">
                    <input type="checkbox" value="5" name="enemyMap[5]" onclick="atacar(this);">
                    <input type="checkbox" value="6" name="enemyMap[6]" onclick="atacar(this);">
                    </br>                       
                    <input type="checkbox" value="7" name="enemyMap[7]" onclick="atacar(this);">
                    <input type="checkbox" value="8" name="enemyMap[8]" onclick="atacar(this);">
                    <input type="checkbox" value="9" name="enemyMap[9]" onclick="atacar(this);">
                    <input type="checkbox" value="10" name="enemyMap[10]" onclick="atacar(this);">
                    <input type="checkbox" value="11" name="enemyMap[11]" onclick="atacar(this);">
                    <input type="checkbox" value="12" name="enemyMap[12]" onclick="atacar(this);">
                    </br>                        
                    <input type="checkbox" value="13" name="enemyMap[13]" onclick="atacar(this);">
                    <input type="checkbox" value="14" name="enemyMap[14]" onclick="atacar(this);">
                    <input type="checkbox" value="15" name="enemyMap[15]" onclick="atacar(this);">
                    <input type="checkbox" value="16" name="enemyMap[16]" onclick="atacar(this);">
                    <input type="checkbox" value="17" name="enemyMap[17]" onclick="atacar(this);">
                    <input type="checkbox" value="18" name="enemyMap[18]" onclick="atacar(this);">
                    </br>                        
                    <input type="checkbox" value="19" name="enemyMap[19]" onclick="atacar(this);">
                    <input type="checkbox" value="20" name="enemyMap[20]" onclick="atacar(this);">
                    <input type="checkbox" value="21" name="enemyMap[21]" onclick="atacar(this);">
                    <input type="checkbox" value="22" name="enemyMap[22]" onclick="atacar(this);">
                    <input type="checkbox" value="23" name="enemyMap[23]" onclick="atacar(this);">
                    <input type="checkbox" value="24" name="enemyMap[24]" onclick="atacar(this);">
                    </br>                        
                    <input type="checkbox" value="25" name="enemyMap[25]" onclick="atacar(this);">
                    <input type="checkbox" value="26" name="enemyMap[26]" onclick="atacar(this);">
                    <input type="checkbox" value="27" name="enemyMap[27]" onclick="atacar(this);">
                    <input type="checkbox" value="28" name="enemyMap[28]" onclick="atacar(this);">
                    <input type="checkbox" value="29" name="enemyMap[29]" onclick="atacar(this);">
                    <input type="checkbox" value="30" name="enemyMap[30]" onclick="atacar(this);">
                    </br>                        
                    <input type="checkbox" value="31" name="enemyMap[31]" onclick="atacar(this);">
                    <input type="checkbox" value="32" name="enemyMap[32]" onclick="atacar(this);">
                    <input type="checkbox" value="33" name="enemyMap[33]" onclick="atacar(this);">
                    <input type="checkbox" value="34" name="enemyMap[34]" onclick="atacar(this);">
                    <input type="checkbox" value="35" name="enemyMap[35]" onclick="atacar(this);">
                    <input type="checkbox" value="36" name="enemyMap[36]" onclick="atacar(this);">
                </div>
                <br>
                <label>Console</label>
                <br>
                <textarea id="console" rows="8" cols="50" style="margin-bottom: 10px;"></textarea>
            </div>
        </div>

    <?php include 'import/scripts.html'; ?>
    <script type="text/javascript">
        var realizarAtaque = true;
        var fimDeJogo = false;
        var intervalId = setInterval(verificaTurno, 6000);

        function verificaTurno() {
            $.get("requests/verificaTurno.php?name=" + $('#name').val(), function (data) {
                //Sendo a vez do jogador, imprime mensagem para realizar ataque
                if (data === "true" && realizarAtaque && !fimDeJogo) {
                    $('#console').prepend('É a sua vez! Realize um ataque!\n');
                    realizarAtaque = false;

                    //Reabilita mapa para atacar o inímigo
                    $("#enemyMap :input").attr("disabled", null);
                }

                verificaNotificacoes();
                verificaFimDeJogo();
            });
        }

        function verificaNotificacoes() {
            $.get("requests/notificacoesDeJogo.php?name=" + $('#name').val(), function (data) {
                //Verifica se possui algum tipo de notificação
                if (Boolean(data)) {
                    //Caso seja notificação do tipo "acertado" escreva em console que o jogador foi atingido
                    if (data === "acertado") {
                        $("#console").prepend("Você foi acertado!\n");
                    }
                }
            })
        }

        function exibeMapaDoJogador() {
            $.get("requests/mapaJogador.php?name=" + $('#name').val(), function (data) {
                var mapa = data.split("-");

                //Marca o mapa do jogador com as checkbox que ele selecionou
                $.each(mapa, function () {
                    $("#yourMap :input[value = "+ this +"]").prop("checked", true);
                })
            })
        }

        function atacar(x) {
            $.get("requests/atacar.php?name=" + $('#name').val() + "&value=" + $(x).val(), function (data) {
                //Verifia se o jogador acertou ou erro o alvo
                if (data === "true") {
                    $('#console').prepend('Você acertou ele!\n');
                } else{
                    $('#console').prepend('Você errou!\n');
                }

                //Desabilita mapa até próximo turno
                $("#enemyMap :input").attr("disabled", true);

                //Reabilita impressão da mensagem para realizar ataque
                realizarAtaque = true;
            });
        }

        function setFimDeJogo() {
            intervalId = clearInterval(intervalId);
            fimDeJogo = true;
        }

        function verificaFimDeJogo() {
            //Fim de jogo para execução de verificação
            if (fimDeJogo) {
                return -1;
            }

            $.get("requests/fimDeJogo.php?name=" + $('#name').val(), function (data) {
                //Verifica se existe algum ganhador.
                if (data !== "false") {
                    //Define fim de jogo
                    setFimDeJogo();

                    //Verifica se jogador que ganho a partida ou foi o adversário
                    if (data == $('#name').val()) {
                        $("#console").prepend('Fim de jogo! Você venceu a partida\n');
                    } else {
                        $("#console").prepend('Fim de jogo! O oponente ' + data + ' venceu a partida\n');
                    }

                    //Desabilita os inputs
                    $("#enemyMap :input").attr("disabled", true);
                }
            })
        }

        // Quando carregar a página
        $(function () {
            //Exibi o mapa marcado pelo jogador
            exibeMapaDoJogador();

            //Desabilita inputs até verificação de turnos
            $("#yourMap :input").attr("disabled", true);
            $("#enemyMap :input").attr("disabled", true);

            //Agurade início da partida
            $("#console").prepend("Aguarde o início da partida...")
        });
    </script>
</body>
</html>