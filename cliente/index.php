<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include 'import/head.html'; ?>
    </head>
    <body>
        <div id="outer">  
            <div id="inner">
                <form id="registerForm">
                    <div class="box">
                        <div class="form-group"> 
                            <label for="name">Insira seu nome:</label> 
                            <input type="text" placeholder="Nome do jogador" id="name" name="name" class="form-control"> 
                        </div>
                        <label>Escolha seu mapa:</label>
                        <div class="map" id="yourMap">
                            <input type="checkbox" name="yourMap[1]">
                            <input type="checkbox" name="yourMap[2]">
                            <input type="checkbox" name="yourMap[3]">
                            <input type="checkbox" name="yourMap[4]">
                            <input type="checkbox" name="yourMap[5]">
                            <input type="checkbox" name="yourMap[6]">
                            </br>
                            <input type="checkbox" name="yourMap[7]">
                            <input type="checkbox" name="yourMap[8]">
                            <input type="checkbox" name="yourMap[9]">
                            <input type="checkbox" name="yourMap[10]">
                            <input type="checkbox" name="yourMap[11]">
                            <input type="checkbox" name="yourMap[12]">
                            </br>
                            <input type="checkbox" name="yourMap[13]">
                            <input type="checkbox" name="yourMap[14]">
                            <input type="checkbox" name="yourMap[15]">
                            <input type="checkbox" name="yourMap[16]">
                            <input type="checkbox" name="yourMap[17]">
                            <input type="checkbox" name="yourMap[18]">
                            </br>
                            <input type="checkbox" name="yourMap[19]">
                            <input type="checkbox" name="yourMap[20]">
                            <input type="checkbox" name="yourMap[21]">
                            <input type="checkbox" name="yourMap[22]">
                            <input type="checkbox" name="yourMap[23]">
                            <input type="checkbox" name="yourMap[24]">
                            </br>
                            <input type="checkbox" name="yourMap[25]">
                            <input type="checkbox" name="yourMap[26]">
                            <input type="checkbox" name="yourMap[27]">
                            <input type="checkbox" name="yourMap[28]">
                            <input type="checkbox" name="yourMap[29]">
                            <input type="checkbox" name="yourMap[30]">
                            </br>
                            <input type="checkbox" name="yourMap[31]">
                            <input type="checkbox" name="yourMap[32]">
                            <input type="checkbox" name="yourMap[33]">
                            <input type="checkbox" name="yourMap[34]">
                            <input type="checkbox" name="yourMap[35]">
                            <input type="checkbox" name="yourMap[36]">
                        </div>
                        <button class="btn btn-default pull-right" id="registrar" type="button">Entrar</button>
                    </div>
                </form>
            </div>
        </div>

        <?php include 'import/scripts.html'; ?>
        <script type="text/javascript">
            $('#registrar').on('click', function () {
                //Obtém o número de checkboxs marcadas
                var quantidadeMarcada = document.querySelectorAll('input[type="checkbox"]:checked').length;

                //Condições para início do jogo
                var quantidadeMinimaMarcada = (quantidadeMarcada > 0) && (quantidadeMarcada <= 11);
                var nomeNaoVazio = Boolean($("#name").val());

                //Registra jogador caso condições validas
                if (quantidadeMinimaMarcada && nomeNaoVazio) {
                    $.post("requests/registrar.php", $("#registerForm").serialize()).done(function (data) {
                        if (data === "true") {
                            $(location).attr('href', 'listaJogadores.php?name=' + $('#name').val());
                        }
                    });
                } else {
                    //Imprime mensagem de erro
                    console.log("Erro ao registrar jogador, verifique o nome registrado e a quantidade de checkboxs marcadas");
                }
            });
        </script>
    </body>
</html>