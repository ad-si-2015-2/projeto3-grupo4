<h1>Projeto 3</h1>
Continuação do projeto 1 e 2 - Batalha Naval
=================================================================================
Descrição do Projeto 3: Programação de jogo multiusuário com Web Services (SOAP)
---------------------------------------------------------------------------------

Descrição do Jogo
----------------------------

Batalha naval é um jogo de tabuleiro de dois jogadores, no qual os jogadores têm de adivinhar em que quadrados estão os navios do oponente. Seu objetivo é derrubar os barcos do oponente adversário, e ganha quem derrubar todos os navios adversários primeiro.

Regras
----------------------------

Antes do início do jogo, cada jogador coloca os seus navios nos quadros, alinhados horizontalmente ou verticalmente. O número de navios permitidos é igual para ambos os jogadores e os navios não podem se sobrepor.

Após os navios terem sido posicionados, o jogo continua numa série de turnos. Em cada turno um jogador diz um quadrado na grelha do oponente, se houver um navio nesse quadrado, é colocada uma marca vermelha, se não houver, é colocada uma marca branca.

Os tipos de navios são: porta-aviões (5 quadrados adjacentes em forma de T), os submarinos (1 quadrado apenas), barcos de dois, três e quatro canos. Numa das variações deste jogo, as grelhas são de dimensão 10x10, e o número de navios são: 1, 4, 3, 2, 1, respectivamente.


Documentação do Projeto
------------------------------------------------------------

Toda documentação do projeto 3 se encontra no [Wiki](https://gitlab.com/ad-si-2015-2/projeto3-grupo4/wikis/home).